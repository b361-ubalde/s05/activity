from django.urls import path

from . import views

"""
path()
    Syntax: 
        path(route, view, name)
"""
app_name = "todolist"
urlpatterns = [
    # /todoitem route
    path('', views.index, name='index'),

    # /todoitem/<event_id> route
    path('event_item/<int:event_id>/', views.event_item, name='event_item'),

    # /todoitem/<todoitem_id> route
    path('todoitem/<int:todoitem_id>/', views.todoitem, name='viewtodoitem'),

    # /register route
    path('register/', views.register, name='register'),

    # /change_password route
    path('change_password/', views.change_password, name="change_password"),

    # login route
    path('login/', views.login_view, name="login"),

    # logout route
    path('logout/', views.logout_view, name="logout"),

    # add_task route
    path('add_task/', views.add_task, name="add_task"),

    path('<int:todoitem_id>/', views.todoitem, name="todoitem"),
    path('<int:todoitem_id>/edit/', views.update_task, name="update_task"),
    path('<int:todoitem_id>/delete', views.delete_task, name="delete_task"),

    # add_event route
    path('add_event/', views.add_event, name="add_event"),

    
]
