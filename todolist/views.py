from django.shortcuts import render, redirect, get_object_or_404
from django.http import HttpResponse
from django.template import loader
from django.contrib.auth.models import User
from django.contrib.auth import authenticate, login, logout
from django.forms.models import model_to_dict
from django.utils import timezone

# Local imports
from .models import ToDoItem, Event
from .forms import LoginForm, AddTaskForm, UpdateTaskForm, AddEventForm

# Create your views here.


def index(request):
    todoitem_list = ToDoItem.objects.filter(user_id=request.user.id)
    event_list = Event.objects.filter(user_id=request.user.id)
    context = {
        'todoitem_list': todoitem_list,
        'event_list': event_list
    }
    return render(request, "todolist/index.html", context)


def todoitem(request, todoitem_id):
    # retrieves the item object
    todoitem = get_object_or_404(ToDoItem, pk=todoitem_id)

    # response = "You are viewing the details of %s"
    return render(request, "todolist/todoitem.html", model_to_dict(todoitem))


def register(request):
    # Retrieves all users from the database using User model from Django
    # users now is an array of object that contains user information
    users = User.objects.all()
    is_user_registered = False

    # Context values are passed to other files, usually in the html file (In our context, it will be passed to register.html)
    context = {
        "is_user_registered": is_user_registered
    }

    for indiv_user in users:
        if indiv_user.username == "johndoe":
            is_user_registered = True
            break

    if is_user_registered == False:
        user_to_register = User()
        user_to_register.username = "johndoe"
        user_to_register.first_name = "John"
        user_to_register.last_name = "Doe"
        user_to_register.email = "john@mail.com"

        # Setter function for the password
        user_to_register.set_password("john1234")
        user_to_register.is_staff = False
        user_to_register.is_active = True

        user_to_register.save()

        context = {
            "first_name": user_to_register.first_name,
            "last_name": user_to_register.last_name
        }

    return render(request, "todolist/register.html", context)


def change_password(request):
    is_user_authenticated = False
    user = authenticate(username="johndoe", password="john1234")
    print(user)

    #
    if user is not None:
        authenticated_user = User.objects.get(username="johndoe")
        authenticated_user.set_password("johndoe1")
        authenticated_user.save()
        is_user_authenticated = True

    context = {
        "is_user_authenticated": is_user_authenticated
    }
    return render(request, "todolist/change_password.html", context)


def login_view(request):
    # username = "johndoe"
    # password = "johndoe1"
    # user = authenticate(username=username, password=password)

    context = {
        # "is_user_authenticated": False
    }

    if request.method == 'POST':

        form = LoginForm(request.POST)

        if form.is_valid() == False:
            # Returns a blank Login Form
            form = LoginForm()
        else:
            # Retrieve information from the Login Form
            username = form.cleaned_data['username']
            password = form.cleaned_data['password']
            user = authenticate(username=username, password=password)
            context = {
            "username": username,
            "password": password
            }

        if user is not None:
            # Saves the user id in the session using Django's Session framework
            login(request, user)
            return redirect("todolist:index")
        else:
            # Provide context with Error to conditionally render error message
            context = {
                "error": True
            }

    return render(request, "todolist/login.html", context)


def logout_view(request):
    # Removes the saved session upon login
    logout(request)
    return redirect("todolist:index")

def add_task(request):
    context = {}

    if request.method == 'POST':
        form = AddTaskForm(request.POST)

        if form.is_valid() == False:
            form = AddTaskForm()
        else:
            task_name = form.cleaned_data['task_name']
            description = form.cleaned_data['description']

            # Check the database if a task already exists
            duplicates = ToDoItem.objects.filter(task_name=task_name)

            # If todoitem does not contain anny duplicates
            if not duplicates:

                ToDoItem.objects.create(task_name=task_name, description=description, date_created=timezone.now(), user_id=request.user.id)

            else:
                context = {
                    "error": True
                }

    return render(request, "todolist/add_task.html", context)


def update_task(request, todoitem_id):
    todoitem = ToDoItem.objects.filter(pk=todoitem_id)

    context = {
        "user": request.username,
        "todoitem_id": todoitem_id,
        "task_name": todoitem[0].task_name,
        "description": todoitem[0].description,
        "status": todoitem[0].status
    }

    if request.method == 'POST':
        form = UpdateTaskForm(request.POST)

        if form.is_valid == False:
            form = UpdateTaskForm()
        else:
            task_name = form.cleaned_data['task_name']
            description = form.cleaned_data['description']
            status = form.cleaned_data['status']

            if todoitem:
                todoitem[0].task_name = task_name
                todoitem[0].description = description
                todoitem[0].status = status

                todoitem[0].save()
                return redirect("todoitem:index")

            else:
                context = {
                    "error": True
                }
    return render(request, "todolist/update_task.html", context)


def delete_task(request,todoitem_id):

    todoitem = ToDoItem.objects.filter(pk=todoitem_id).delete()
    return redirect("todolist:index")

def add_event(request):
    context = {}

    if request.method == 'POST':
        form = AddEventForm(request.POST)

        if form.is_valid() == False:
            form = AddEventForm()

        else:
            event_name = form.cleaned_data['event_name']
            description = form.cleaned_data['description']

            # Check if there are duplicates
            duplicates = Event.objects.filter(event_name=event_name)

            # If Event does not have anny duplicates in the database
            if not duplicates:
                Event.objects.create(event_name=event_name, description=description, date_created=timezone.now(), user_id=request.user.id)
            else:
                context = {
                    "error": True
                }
    return render(request, "todolist/add_event.html", context)

def event_item(request, event_id):
    event_item = get_object_or_404(Event, pk=event_id)
    return render(request, "todolist/event_item.html",model_to_dict(event_item))